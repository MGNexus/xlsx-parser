<?
class CMGNParseXlsx
{
	private static $slash = "/";
	public $folderName;
	private 
		$filepath,
		$folderPath,
		//паттерны
		$arReg = array(
			'main' 	=> '/<xdr:[one|two]+CellAnchor(.*?)>(.*?)<\/xdr:[one|two]+CellAnchor>/ius',
			'attr' 	=> '/(.+?)=[\'|"](.*?)[\'|"]/ius',
			'from' 	=> '/<xdr:from><xdr:col>(.*)<\/xdr:col><xdr:colOff>(.*)<\/xdr:colOff><xdr:row>(.*)<\/xdr:row><xdr:rowOff>(.*)<\/xdr:rowOff><\/xdr:from>/ius',
			'to'	=> '/<xdr:to><xdr:col>(.*)<\/xdr:col><xdr:colOff>(.*)<\/xdr:colOff><xdr:row>(.*)<\/xdr:row><xdr:rowOff>(.*)<\/xdr:rowOff><\/xdr:to>/ius',
			'pic'	=> '/<xdr:pic>(.*)<\/xdr:pic>/',
			'sp'	=> '/<xdr:sp(.*?)>(.*?)<\/xdr:sp>/ius',
			'cNvPr' => '/<xdr:cNvPr(.*?)\/>/ius',
			'txBody'=> '/<xdr:txBody>(.*?)<\/xdr:txBody>/ius',
			'at'	=> '/<a:t>(.*?)<\/a:t>/ius',
			'shape' => '/<v:shape (.*?)>(.*?)<\/v:shape>/ius',
			'x_simple' => '/<x:#name#>(.*?)<\/x:#name#>/ius',
			
		);
	
	
	public function __construct($filepath)
	{
		$this->filepath = $filepath;
		$arFileName = pathinfo($this->filepath);
		
		$this->folderPath = $arFileName['dirname'].self::$slash.$arFileName['filename'];
		$this->folderName = $arFileName['filename'];
	}
	
	public function unzip()
	{
		if(false !== ($zip = zip_open($this->filepath)))
		{
			if(is_dir($this->folderPath))
				throw new Exception('Папка '.$this->folderPath.' существует');
		
			mkdir($this->folderPath);
			while($zip_entry = zip_read($zip))
			{
				$entry_name = zip_entry_name($zip_entry);
					
				$arPath = explode("/", $entry_name);
		
				$file = array_pop($arPath);
		
				//Создадим папки
				$sPath = $this->folderPath;
				foreach($arPath as $dir)
				{
					$sPath .=self::$slash.$dir;
					if(!is_dir($sPath))
						mkdir($sPath);
				}
		
				//зальем файлы
				$filename = $this->folderPath.self::$slash.str_replace("/", self::$slash, $entry_name);
				if(!is_file($filename))
				{
					$fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
					file_put_contents($filename, $fstream);
				}
			}
		
			zip_close($zip);
		}
	}
	
	public function parse()
	{
		$arDrawing = $this->parseDrawObjects();
		$arShared = $this->parseSharedStrings();
		$arCells = $this->parseList();
		$arCtrl = $this->parseCtrlObjects();
		
		return $this->linker($arCells, $arShared, $arDrawing, $arCtrl);
	}
	
	public function clean()
	{
		$this->removeUnzip($this->folderPath);
		unlink($this->filepath);
	}
	
	private function removeUnzip($path)
	{
		if(is_dir($path))
		{
			$dir = opendir($path);
			while(($subpath = readdir($dir)) !== false)
			{
				if($subpath == '.' || $subpath == '..')
					continue;
				
				$this->removeUnzip($path.self::$slash.$subpath);
			}
			rmdir($path);
		}
		elseif(is_file($path))
		{
			unlink($path);
		}
	}
	
	private function linker(array $arCells, array $arShared, $arDraving = null, $arCtrls = null)
	{
		$arResult = array();
		
		$arTmp = array();
		foreach($arCells as $k => $v)
		{
			list($letter, $digit) = $this->separateLetter($k);
			$arTmp[$k] = array(
				'value' => $arShared[$v],
				'col'	=> $this->getLetterIndex($letter),
				'row'	=> $digit,
			);
		}
		
		$arAdd = array();
		foreach($arTmp as $k=>$v)
		{
			foreach($arTmp as $m => $w)
			{
				if($k === $m) continue;
				
				if(
					$v['row'] == $w['row']&& $w['col']-$v['col'] <=4
					&& !in_array($k, $arAdd) && !in_array($m, $arAdd)
				)
				{
					$arVal = array(
						'name' => $v['value'],
						'code' => $k,
						'value' => $w['value']
					);
					$arAdd[] = $k;
					$arAdd[] = $m;
					
					$arResult[] = $arVal;
				}

			}
		}
		
		if(!empty($arDraving))
		{
			foreach($arDraving as $arObj)
			{
				if(!empty($arObj['sp']))
				{
					$arVal = array(
						'id' 	=> $arObj['sp']['nvSpPr']['@attr']['id'],
						'code'  => $arObj['sp']['nvSpPr']['@attr']['name'],
						'value'	=> $arObj['sp']['t']
					);
					
					foreach($arTmp as $tmp)
					{
						if($tmp['row'] >= $arObj['from']['row'] && $tmp['row'] <= $arObj['to']['row'])
						{
							$arVal['name'] = $tmp['value'];
						}
					}
					$arResult[] = $arVal;
				}
			}
		}
		
		if(!empty($arCtrls))
		{
			foreach($arCtrls as $arObj)
			{
				if(!empty($arObj['FmlaRange']))
				{
					$arVal = array(
						'id' => $arObj['@attr']['id'],
						'code' => $arObj['FmlaRange'],
						'value' => $arObj['Sel'],	
					);
					
					foreach($arTmp as $tmp)
					{
						if($tmp['row'] == $arObj['Row'])
						{
							$arVal['name'] = $tmp['value'];
						}
					}
					$arResult[] = $arVal;
				}
			}
		}
		
		p($arResult);
		
		return $arResult;
	}
	
	private function parseList($page=1)
	{
		$arCells = array();
		$page = intval($page) ? $page : 1;
		$filename = $this->folderPath.self::$slash."xl".self::$slash."worksheets".self::$slash."sheet".$page.".xml";
		$xml = simplexml_load_file($filename);
		
		foreach($xml->sheetData->row as $oRow)
		{
			foreach($oRow->c as $oCell)
			{
				if(!empty($oCell->v))
				{
					$attrs = $oCell->attributes();
					$arCells[(string)$attrs->r] = (string)$oCell->v;
				}
			}
		}
		
		return $arCells;
	}
	
	private function parseSharedStrings()
	{
		$arSharedText = array();
		$filename = $this->folderPath.self::$slash."xl".self::$slash."sharedStrings.xml";
		$xml = simplexml_load_file($filename);
		
		foreach($xml->children() as $oText)
		{
			if(!empty($oText->t))
			{
				$arSharedText[] = (string)$oText->t;
			}
			elseif(!empty($oText->r))
			{
				$arTmp = array(); 
				foreach ($oText->r as $tt)
					$arTmp[] = (string)$tt->t;

				$arSharedText[] = implode('', $arTmp);
			}
		}

		return $arSharedText;
	}
	
	private function parseDrawObjects()
	{
		$filename = $this->folderPath.self::$slash."xl".self::$slash."drawings".self::$slash."drawing1.xml";

		$data = file_get_contents($filename);

		//парсим ноды
		preg_match_all($this->arReg['main'], $data, $arNodes);
		
		$arTree = array();

		foreach($arNodes[0] as $k => $node)
		{
			$arList = array('name' => 'twoCellAnchor');
			$arList['@attr'] = $this->getAttr($arNodes[1][$k]);

			//Позиции
			$arList['from'] = $this->getPos($arNodes[2][$k], 'from');
			$arList['to'] = $this->getPos($arNodes[2][$k], 'to');

			//Найдем и удалим информацию о картинке, чтоб не мешала
			$arNodes[2][$k] = preg_replace($this->arReg['pic'], '', $arNodes[2][$k]);
			
			//Информация о тексте
			preg_match($this->arReg['sp'], $arNodes[2][$k], $arSp);
			
			if(!empty($arSp))
			{
				$arList['sp']['name'] = 'sp';
				$arList['sp']['@attr'] = $this->getAttr($arSp[1]);
				
				//название и id
				$arList['sp']['nvSpPr']['name'] = 'nvSpPr';
				preg_match($this->arReg['cNvPr'], $arSp[2], $arCNvPr);
				
				$arList['sp']['nvSpPr']['@attr'] = $this->getAttr($arCNvPr[1]);
				
				preg_match($this->arReg['txBody'], $arSp[2], $arTxBody);
				preg_match_all($this->arReg['at'], $arTxBody[1], $arAt);
				$arList['sp']['t'] = implode(' ', $arAt[1]);
			}
			
			$arTree[] = $arList;
		}
		return $arTree;
	}
	
	private function parseCtrlObjects()
	{
		$filename = $this->folderPath.self::$slash."xl".self::$slash."drawings".self::$slash."vmlDrawing1.vml";
		$data = file_get_contents($filename);
		
		$arRes = array();
		
		preg_match_all($this->arReg['shape'], $data, $arShapes);
		
		//Парсим поля
		foreach($arShapes[0] as $k =>$arShape)
		{
			$ar['@attr'] = $this->getAttr($arShapes[1][$k]);
			
			foreach(array('Anchor', 'Page', 'FmlaRange','Sel') as $sCode)
			{
				$reg = str_replace("#name#", $sCode, $this->arReg['x_simple']);
				preg_match($reg, $arShapes[2][$k], $arMatch);
				$ar[$sCode] = $arMatch[1];
			}
			
			$arRes[] = $ar;
		}
		
		//Сохраним позицию для категорий
		foreach ($arRes as $v)
			if(strtolower($v['FmlaRange']) == 'направление_обучения')
				$iDist = $v['Sel'];
		
		//Извлечем значения
		foreach($arRes as $k=>$ar)
		{
			if(!empty($ar['FmlaRange']) && !empty($ar['Sel']))
			{
				$rangeName = strtolower($ar['FmlaRange']);
				$arRes[$k]['Sel'] = $this->getCtrlTextVal($rangeName, $rangeName == 'категории_приложение1'?array($ar['Sel'],$iDist):$ar['Sel']);
				
				if(!empty($ar['Anchor']))
				{
					$tmp = explode(',', $ar['Anchor']);
					
					//Согласно наблюдениям - строка для поля хранится в предпоследнем значении. 
					//Значение столбца, предположительно, в первом
					$arRes[$k]['Row'] = trim($tmp[count($tmp)-2]);
				}
			}
		}
		
		return $arRes;
	}
	
	private function getCtrlTextVal($rangeName, $pos)
	{
		$arRangeInfo = array();
		
		$filename = $this->folderPath.self::$slash."xl".self::$slash."workbook.xml";
		$xml = simplexml_load_file($filename);
		
		$rangeName = strtolower($rangeName);
		
		//TODO: Определение страницы сделать по полям
		//Получаем значения для второго листа
		$arCells = $this->parseList(2);
		$arShared = $this->parseSharedStrings();
		
		//Получаем диапозон значений
		$arRangeInfo = $this->getRangeVal($rangeName, $xml);
		
		$arVals = array(0);
		if($rangeName == 'категории_приложение1')
		{
			list($range) = explode(',',$arRangeInfo['FORMULA'][1]);
			$range = $this->separateLetter(str_replace('$','',$range));
			
			//TODO: Разобраться с форлулой и переписать костыль
			$arRangeInfo = $this->getRangeVal('направление_обучения', $xml);
			$arStart = $this->separateLetter($arRangeInfo['START']);
			
			$iCatIndex = $arCells[$this->getLetterByIndex($this->getLetterIndex($arStart[0])-1).($arStart[1]+$pos[1]-1)];
			
			$arVals = array(0);
			foreach($arCells as $k=>$v)
			{
				$sep = $this->separateLetter($k);
				if($sep[0] == $range[0] && $sep[1] >= $range[1] && $v == $iCatIndex)
				{
					$cell = $this->getLetterByIndex($this->getLetterIndex($sep[0])+1).$sep[1];
					$arVals[] =  $arShared[$arCells[$cell]];
				}
			}
			
			return $arVals[$pos[0]];
		}
		else
		{
			if(empty($arRangeInfo['END']))
			{
				return  $arShared[$arCells[$arRangeInfo['START']]];
			}
			else
			{
				$arStart = $this->separateLetter($arRangeInfo['START']);
				return $arShared[$arCells[$arStart[0].($arStart[1]+$pos-1)]];
			}
		}
		
		return false;
	}
	
	private function getRangeVal($rangeName, $oXml)
	{
		foreach ($oXml->definedNames->children() as $oChild)
		{
			$attr = $oChild->attributes();
				
			$curRangeName = strtolower((string)$attr->name);
			
			if($curRangeName == $rangeName)
			{
				$rangeVal = (string)$oChild;
				break;
			}
		}

		$tmp = explode('!', $rangeVal);
		
		if(count($tmp) > 2)
			return array(
				'NAME' => $rangeName,
				'FORMULA' => $tmp
			);
		
		$range = explode(':', $tmp[1]);
		
		foreach($range as $k=>$v)
			$range[$k] = str_replace('$','',$v);

		return array(
			'NAME' => $rangeName,
			'SHEET_NAME' => $tmp[0],
			'START' => $range[0],
			'END' => $range[1],
		);
	}
	
	private function getAttr($str)
	{
		$arRes = array();
		preg_match_all($this->arReg['attr'], $str, $arAttrs);
		foreach($arAttrs[0] as $kk => $arAt)
		{
			$arRes[trim($arAttrs[1][$kk])] = trim($arAttrs[2][$kk]);
		}
		
		return $arRes;
	}
	
	private function getPos($str, $name)
	{
		preg_match($this->arReg[$name], $str, $arPos);
		return  array(
				'name'	 => $name,
				'col'	 => $arPos[1],
				'colOff' => $arPos[2],
				'row' 	 => $arPos[3],
				'rowOff' => $arPos[4]
		);
	}
	
	private function getLetterIndex($sLetter)
	{
		$sLetterMap = '_abcdefghijklmnopqrstuvwxyz';
		
		$sLetter = trim(strtolower($sLetter));
		$iLen = strlen($sLetter);
		$pos = 0;
		
		for($i=0; $i<$iLen; $i++)
		{
			$c = substr($sLetter, $i, 1);
			$c_pos = strpos($sLetterMap, $c);

			if($iLen === 1)
				$pos += $c_pos;
			else 
				$pos += pow(26, $iLen-$i-1)*$c_pos;
		}
		
		return $pos;
	}
	
	private function getLetterByIndex($index)
	{
		$sLetterMap = '_abcdefghijklmnopqrstuvwxyz';
		
		$sRes = '';
		for($i=3; $i>=0; $i--)
		{
			$mod = floor($index/pow(26,$i));
			if($mod) 
			{
				$sRes .= substr($sLetterMap, $mod, 1);
				$index -= pow(26,$i);
			}
		}
		
		return strtoupper($sRes);
	}
	
	private function separateLetter($letter)
	{
		$pos = 0;
		do
		{
			$c = substr($letter, $pos, 1);
			$pos++;
		}
		while(intval($c));
		
		return array(
			substr($letter,0,$pos),
			substr($letter, $pos)
		);
	}
} 
?>